object vMain: TvMain
  Left = 0
  Top = 0
  ActiveControl = ButtonExe
  Caption = 'png to bitmap'
  ClientHeight = 341
  ClientWidth = 627
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object RzStatusBar1: TRzStatusBar
    Left = 0
    Top = 322
    Width = 627
    Height = 19
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdTop, sdRight, sdBottom]
    BorderWidth = 0
    TabOrder = 0
    object RzVersionInfoStatus1: TRzVersionInfoStatus
      AlignWithMargins = True
      Left = 523
      Top = 0
      Width = 89
      Height = 19
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 15
      Margins.Bottom = 0
      Align = alRight
      AutoSize = True
      Field = vifFileVersion
      VersionInfo = RzVersionInfo1
      ExplicitLeft = 527
      ExplicitHeight = 22
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 627
    Height = 322
    ActivePage = TabConvert
    Align = alClient
    Style = tsButtons
    TabOrder = 1
    object TabDrag: TTabSheet
      Caption = 'TabDrag'
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 619
        Height = 288
        Align = alClient
        Alignment = taCenter
        Caption = 'Drag the png files then drop hear :)'
        Layout = tlCenter
        ExplicitWidth = 204
        ExplicitHeight = 16
      end
    end
    object TabConvert: TTabSheet
      Caption = 'TabConvert'
      ImageIndex = 1
      object RzSplitter1: TRzSplitter
        Left = 0
        Top = 57
        Width = 619
        Height = 231
        Position = 314
        Percent = 51
        UsePercent = True
        HotSpotVisible = True
        HotSpotDirection = hsdBoth
        SplitterWidth = 7
        Align = alClient
        TabOrder = 0
        BarSize = (
          314
          0
          321
          231)
        UpperLeftControls = (
          ListBoxPng)
        LowerRightControls = (
          ListBoxBmp)
        object ListBoxPng: TListBox
          Left = 0
          Top = 0
          Width = 314
          Height = 231
          Align = alClient
          TabOrder = 0
        end
        object ListBoxBmp: TListBox
          Left = 0
          Top = 0
          Width = 298
          Height = 231
          Align = alClient
          TabOrder = 0
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 619
        Height = 57
        Align = alTop
        TabOrder = 1
        DesignSize = (
          619
          57)
        object LabelCnt: TLabel
          Left = 551
          Top = 12
          Width = 19
          Height = 16
          Anchors = [akTop, akRight]
          Caption = '0/0'
        end
        object ButtonExe: TButton
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Execute'
          TabOrder = 0
          OnClick = ButtonExeClick
        end
        object ProgressBar: TProgressBar
          Left = 89
          Top = 12
          Width = 456
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          Step = 1
          TabOrder = 1
        end
        object RadioDeleteFile: TRzRadioButton
          Left = 24
          Top = 36
          Width = 188
          Height = 18
          Caption = 'Delete the BMP file if it exists.'
          Checked = True
          TabOrder = 2
          TabStop = True
          Transparent = True
        end
        object RadioButton2: TRzRadioButton
          Left = 232
          Top = 36
          Width = 195
          Height = 18
          Caption = 'Rename the BMP file if it exists'
          TabOrder = 3
          Transparent = True
        end
      end
    end
  end
  object RzVersionInfo1: TRzVersionInfo
    Left = 112
    Top = 104
  end
end
